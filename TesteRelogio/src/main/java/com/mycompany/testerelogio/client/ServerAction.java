package com.mycompany.testerelogio.client;

import com.mycompany.testerelogio.model.Client;
import com.mycompany.testerelogio.model.Relogio;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerAction {
    
    public static void main(String[] args) throws IOException {
        
        ArrayList<Client> clients = new ArrayList<Client>();
        clients.add(new Client("localhost:3000".split(":")));
        clients.add(new Client("localhost:3001".split(":")));
        clients.add(new Client("localhost:3002".split(":")));
        
        Relogio time = new Relogio();
        long updateCounter = 0;
        while(true){
            Socket echoSocket = null;
            PrintWriter out = null;
            BufferedReader in = null;

            for(Client client:clients){
                boolean erro = false;
                try {
                    echoSocket = new Socket(client.getHost(),client.getPorta() );

                    out = new PrintWriter(echoSocket.getOutputStream(), true);
                    in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));

                } catch (UnknownHostException e) {
                    System.err.println("Don't know about host: " + client.getHost());
                    erro = true;
                } catch (IOException e) {
                    System.err.println("Couldn't get I/O for " + "the connection to: " + client.getHost());
                    erro = true;
                }
                if(erro){
                    client.connectionWasSuccessfull = false;
                }
                else{
                    client.connectionWasSuccessfull = true;
                    long inicio = time.getRelogio();
                    out.println(String.valueOf(time.getRelogio()));
                    String error = in.readLine();
                    long fim = time.getRelogio();
                    long rtt = fim - inicio;
                    client.erroAtual = Long.parseLong(error) - (rtt/2);                
                    out.close();
                    in.close();
                    echoSocket.close();
                }

            }

            int counter = 1;
            long sum = 0;
            for(Client client : clients){
                if(client.connectionWasSuccessfull){
                    sum += client.erroAtual;
                    counter++;
                }
            }

            long deviation = sum/counter;
            time.ajustaRelogio(deviation);


            echoSocket = null;
            out = null;

            for(Client client:clients){
                if(client.connectionWasSuccessfull){
                    boolean erro = false;
                    try {
                        echoSocket = new Socket(client.getHost(),client.getPorta() );

                        out = new PrintWriter(echoSocket.getOutputStream(), true);

                    } catch (UnknownHostException e) {
                        System.err.println("Don't know about host: " + client.getHost());
                        erro = true;
                    } catch (IOException e) {
                        System.err.println("Couldn't get I/O for " + "the connection to: " + client.getHost());
                        erro = true;
                    }
                    if(!erro){
                        out.println("#" + String.valueOf(deviation - client.erroAtual));
                        out.close();
                        in.close();
                        echoSocket.close();
                    }
                }
            }
            
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                System.out.println("Erro de Sleep.");
            }
            
            echoSocket = null;
            out = null;
            
            if((updateCounter % 7) == 0){
                long schedule = 0;
                for(Client client:clients){
                    boolean erro = false;
                    try {
                        echoSocket = new Socket(client.getHost(),client.getPorta() );

                        out = new PrintWriter(echoSocket.getOutputStream(), true);

                    } catch (UnknownHostException e) {
                        System.err.println("Don't know about host: " + client.getHost());
                        erro = true;
                    } catch (IOException e) {
                        System.err.println("Couldn't get I/O for " + "the connection to: " + client.getHost());
                        erro = true;
                    }
                    if(!erro){
                        schedule = time.getRelogio() + 10000;
                        out.println("$" + String.valueOf(schedule));
                        out.close();
                        in.close();
                        echoSocket.close();
                    }     
                }
                try {
                    Thread.sleep(schedule - time.getRelogio());
                } catch (InterruptedException ex) {
                    System.out.println("Erro de Sleep.");
                }
                System.out.println(time.getRelogio());
                System.out.println(time.getDate());
                System.out.println(System.currentTimeMillis());
            }

            
            updateCounter++;
            
        }

    }
    
    
}
