package com.mycompany.testerelogio.server;

import com.mycompany.testerelogio.model.Relogio;
import java.net.*;
import java.io.*;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientAction{
    
    public static void main(String[] args) throws IOException{
        Relogio time = new Relogio();
        Random rnd = new Random();
        ServerSocket serverSocket = null;
                     int porta = 3002;

        try {
             serverSocket = new ServerSocket(porta);
            }
        catch (IOException e)
            {
             System.err.println("Could not listen on port: " + porta);
             System.exit(1);
            }

        Socket clientSocket = null;
        while(true){

            try {
                 clientSocket = serverSocket.accept();
                }
            catch (IOException e)
                {
                 System.err.println("Accept failed.");
                 System.exit(1);
                }


            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(),
                                              true);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader( clientSocket.getInputStream()));

            String inputLine;
            
            inputLine = in.readLine();
            if(inputLine.charAt(0) == '#'){
                inputLine = inputLine.substring(1);
                time.ajustaRelogio(Long.parseLong(inputLine));
            }
            else if(inputLine.charAt(0) == '$'){
                try {
                    Thread.sleep(Long.parseLong(inputLine.substring(1)) - time.getRelogio());
                } catch (InterruptedException ex) {
                    System.out.println("Erro de Sleep.");
                }
                System.out.println(time.getRelogio());
                System.out.println(time.getDate());
                System.out.println(System.currentTimeMillis());
            }
            else{
                
                long erro = time.getRelogio() - Long.parseLong(inputLine); 
                String erroStr = String.valueOf(erro);
                System.out.println (erroStr); 
                
                out.println(erroStr);
            }
            out.close();
            in.close();
            clientSocket.close();
        }
   }
}
