package com.mycompany.testerelogio.model;

import java.util.Date;
import java.util.Random;

public class Relogio {
    private long erro;
    Random rng;

    public Relogio () {
        rng = new Random();
        erro = rng.nextInt(30)*1000;
    }

    public long getRelogio () {
        return (System.currentTimeMillis() + erro);
    }

    public void ajustaRelogio (long a) {
        erro += a;
    }

    public Date getDate () {
        return new Date(System.currentTimeMillis()+erro);
    }

}