package com.mycompany.testerelogio;

import com.mycompany.testerelogio.model.Relogio;

public class TestaRelogio {

    public static void main(String[] args) {
        Relogio r = new Relogio();
        System.out.println(r.getRelogio());
        System.out.println(r.getDate());
        r.ajustaRelogio(5000);
        System.out.println(r.getRelogio());
        System.out.println(r.getDate());
    }

}

